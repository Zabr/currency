<?php

return [
    'default_error' => 'Error not specified.',
    'undefined_error' => 'Undefined error.',
    'validation_error' => 'Validation error.',
    'unauthenticated_error' => 'Log in to do this request.',
    'authenticated_error' => 'Log out to do this request.',
    'validation_fields_not_found_error' => 'Validation rules not found for some fields.',
    'incorrect_credentials_error' => 'Invalid credentials.'
];
