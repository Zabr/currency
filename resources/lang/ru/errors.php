<?php

return [
    'default_error' => 'Ошибка не указана.',
    'undefined_error' => 'Непредвиденная ошибка.',
    'validation_error' => 'Ошибка валидации.',
    'unauthenticated_error' => 'Войдите, чтобы выполнить этот запрос.',
    'authenticated_error' => 'Выйдите, чтобы выполнить этот запрос.',
    'validation_fields_not_found_error' => 'Не найдены правила валидации для некоторых полей.',
    'incorrect_credentials_error' => 'Не верный логин или пароль.'
];
