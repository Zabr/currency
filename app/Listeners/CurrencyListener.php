<?php

namespace App\Listeners;

use App\Cache\BaseRepositoryCacheKeys;
use App\Cache\CurrencyRepositoryCacheKeys;
use App\Events\OnCurrencyImportEndsEvent;
use App\Models\Currency;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Cache;

class CurrencyListener
{
    use BaseRepositoryCacheKeys;
    use CurrencyRepositoryCacheKeys;

    public function subscribe(Dispatcher $events): void
    {
        $events->listen(
            OnCurrencyImportEndsEvent::class,
            [$this, 'onCurrencyImportEndsListener']
        );
    }

    public function onCurrencyImportEndsListener(OnCurrencyImportEndsEvent $event): void
    {
        if (!empty($event->dates)) {
            foreach ($event->dates as $date) {
                Cache::forget($this->whereCacheKey(Currency::class, 'date', $date));
            }
        }

        if (!empty($event->codes)) {
            foreach ($event->codes as $code) {
                Cache::forget($this->whereCacheKey(Currency::class, 'code', $code));
            }
        }

        Cache::tags($this->selectCacheTag())->flush();
    }
}
