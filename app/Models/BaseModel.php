<?php

namespace App\Models;

use App\Traits\Models\FillsUuid;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use FillsUuid;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';
}
