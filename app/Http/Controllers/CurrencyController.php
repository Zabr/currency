<?php

namespace App\Http\Controllers;

use App\Presenters\Currency\CurrencyPresenter;
use App\Requests\Currency\CurrencyHistoryRequest;
use App\Services\Contracts\CurrencyServiceContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class CurrencyController extends Controller
{
    public function current(): JsonResponse
    {
        $currencies = app(CurrencyServiceContract::class)->current();

        return response()->apiResponse(CurrencyPresenter::arrayPresent($currencies));
    }

    public function history(CurrencyHistoryRequest $request): JsonResponse
    {
        $currencies = app(CurrencyServiceContract::class)->history($request->validated());

        return response()->apiResponse(CurrencyPresenter::arrayPresent($currencies));
    }
}
