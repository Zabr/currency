<?php

namespace App\Http\Controllers;

use App\Presenters\Auth\UserWithTokenPresenter;
use App\Requests\Auth\LoginRequest;
use App\Requests\Auth\RegistrationRequest;
use App\Services\Contracts\AuthServiceContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    public function register(RegistrationRequest $request): JsonResponse
    {
        $user = app(AuthServiceContract::class)->register($request->validated());

        return response()->apiResponse(UserWithTokenPresenter::present($user));
    }

    public function login(LoginRequest $request): JsonResponse
    {
        $user = app(AuthServiceContract::class)->login($request->validated());

        return response()->apiResponse(UserWithTokenPresenter::present($user));
    }

    public function logout(): JsonResponse
    {
        app(AuthServiceContract::class)->logout(auth()->user());

        return response()->apiResponse();
    }
}
