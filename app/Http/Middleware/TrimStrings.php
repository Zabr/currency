<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TrimStrings as Middleware;

class TrimStrings extends Middleware
{
    /**
     * The attributes that should not be trimmed.
     *
     * @var array
     */
    protected $except = [
        'password' => true,
        'password_confirmation' => true,
    ];

    /**
     * Transform the given value.
     *
     * @param string $key
     * @param mixed  $value
     */
    protected function transform($key, $value): mixed
    {
        if (!empty($this->except[$key])) {
            return $value;
        }

        return is_string($value) ? trim($value) : $value;
    }
}
