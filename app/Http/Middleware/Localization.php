<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Localization
{
    /**
     * Handle an incoming request.
     * @param  Request $request
     * @param  Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if ($request->has('lang')) {
            $lang = $request->get('lang');

            $availableLocales = config('app.available_locales');
            if (!empty($availableLocales[$lang])) {
                app()->setLocale($lang);
            }
        }

        return $next($request);
    }
}
