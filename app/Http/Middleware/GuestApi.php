<?php

namespace App\Http\Middleware;

use App\Exceptions\Auth\AuthenticatedException;
use Closure;
use Illuminate\Http\Request;

class GuestApi
{
    /**
     * Handle an incoming request.
     * @param  Request     $request
     * @param  Closure(Request): (Response|JsonResponse)  $next
     * @param  string|null ...$guards
     *
     * @return mixed
     *
     * @throws AuthenticatedException
     */
    public function handle(Request $request, Closure $next, ...$guards): mixed
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (auth($guard)->check()) {
                throw new AuthenticatedException();
            }
        }

        return $next($request);
    }
}
