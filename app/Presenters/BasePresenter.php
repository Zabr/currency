<?php

namespace App\Presenters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class BasePresenter
{
    abstract protected static function presentItem($data): array;

    public static function present(Model|array $item): array
    {
        if ($item instanceof Model) {
            $item = $item->toArray();
        }

        return static::presentItem($item);
    }

    public static function arrayPresent(Collection|array $items): array
    {
        if ($items instanceof Collection) {
            $items = $items->toArray();
        }

        return array_map(function ($item) {
            return static::presentItem($item);
        }, $items);
    }
}
