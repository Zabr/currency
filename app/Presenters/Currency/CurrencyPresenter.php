<?php

namespace App\Presenters\Currency;

use App\Presenters\BasePresenter;
use Carbon\Carbon;

class CurrencyPresenter extends BasePresenter
{
    protected static function presentItem($data): array
    {
        return [
            'code' => $data['code'],
            'date' => self::formatDate($data['date']),
            'value' => $data['value'],
        ];
    }

    private static function formatDate($date): ?string
    {
        if (empty($date)) {
            return null;
        }

        return Carbon::parse($date)->format('d.m.Y');
    }
}
