<?php

namespace App\Presenters\Auth;

use App\Presenters\BasePresenter;

class UserWithTokenPresenter extends BasePresenter
{
    protected static function presentItem($data): array
    {
        return [
            'id' => $data['id'],
            'login' => $data['login'],
            'token' => $data['token']->plainTextToken,
        ];
    }
}
