<?php

namespace App\Services;

use App\Repositories\Contracts\CurrencyRepositoryContract;
use App\Services\Contracts\CurrencyServiceContract;
use Illuminate\Database\Eloquent\Collection;

class CurrencyService implements CurrencyServiceContract
{
    public function current(): Collection
    {
        return app(CurrencyRepositoryContract::class)->where('date', now()->format('Y-m-d'));
    }

    public function history(array $data): Collection
    {
        return app(CurrencyRepositoryContract::class)->select($data);
    }
}
