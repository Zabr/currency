<?php

namespace App\Services;

use App\Events\OnCurrencyImportEndsEvent;
use App\Repositories\Contracts\CBRepositoryContract;
use App\Repositories\Contracts\CurrencyRepositoryContract;
use App\Services\Contracts\CurrencyImportServiceContract;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Carbon\CarbonPeriod;
use Exception;
use Illuminate\Support\Str;

class CurrencyImportService implements CurrencyImportServiceContract
{
    private array $insert;

    private array $dates;

    private array $codes;

    private array $config;

    public function __construct()
    {
        $this->config = config('cb');
    }

    public function importFromCB(): void
    {
        $cbCurrenciesList = xmlToArray(app(CBRepositoryContract::class)->getDaily());

        $dateEnd = Carbon::now()->subHours(3)->addDay()->startOfDay();

        $this->insert = [];
        $this->dates = [];
        $this->codes = [];
        foreach ($cbCurrenciesList['Valute'] as $cbCurrency) {
            if (empty($cbCurrency['CharCode'])) {
                continue;
            }

            $cbCurrency['CharCode'] = mb_strtolower($cbCurrency['CharCode']);

            // вынужденные запросы в цикле по коду валюты,
            // чтобы не хранить все 320к элементов в оперативной памяти,
            // а очищать её на каждой валюте
            $localCurrencies = app(CurrencyRepositoryContract::class)->where('code', $cbCurrency['CharCode']);
            $localCurrencies = $localCurrencies->keyBy('date');

            $dateStart = Carbon::createFromFormat(
                $this->config['format']['request'],
                $this->config['date_start'][$cbCurrency['CharCode']] ?? $this->config['date_start']['default']
            )->startOfDay();
            $period = CarbonPeriod::create($dateStart, $dateEnd);

            $tempDateStart = null;
            $tempDateEnd = null;
            foreach ($period as $date) {
                if (empty($localCurrencies[(string) $date])) {
                    if (empty($tempDateStart)) {
                        $tempDateStart = $date->clone();
                    }

                    $tempDateEnd = $date;

                    continue;
                }

                // мы будем попадать сюда, пока не начнётся новый цикл,
                // всё это время метки будут null, так что запросов в ЦБ не будет
                $this->getAndParsePeriod($cbCurrency, $tempDateStart, $tempDateEnd);

                $tempDateStart = null;
                $tempDateEnd = null;
            }

            $this->getAndParsePeriod($cbCurrency, $tempDateStart, $tempDateEnd);

            // на текущий день, в 1 валюте может быть менее 11к записей
            // условие сделано с запасом
            if (count($this->insert) > 5000) {
                $this->performInsert();
            }
        }

        if (!empty($this->insert)) {
            $this->performInsert();
        }

        event(new OnCurrencyImportEndsEvent($this->dates, $this->codes));
    }

    private function getAndParsePeriod(array $cbCurrency, ?CarbonInterface $dateStart, ?CarbonInterface $dateEnd): void
    {
        if (empty($dateStart) || empty($dateEnd)) {
            return;
        }

        $dateStartNew = $dateStart->clone()->subMonth();

        // делаем паузу перед каждым запросом
        sleep(1);

        $cbCurrencyValues = xmlToArray(
            app(CBRepositoryContract::class)->getPeriod(
                $cbCurrency['@attributes']['ID'],
                $dateStartNew->format($this->config['format']['request']),
                $dateEnd->format($this->config['format']['request'])
            )
        );

        if (empty($cbCurrencyValues['Record'])) {
            return;
        }

        $cbCurrencyValues = collect($cbCurrencyValues['Record'])->keyBy('@attributes.Date');

        $period = CarbonPeriod::create($dateStartNew, $dateEnd);

        $temp = null;
        foreach ($period as $date) {
            $dateFormatted = $date->format($this->config['format']['response']);

            if (!empty($cbCurrencyValues[$dateFormatted]['Value'])) {
                $value = $cbCurrencyValues[$dateFormatted]['Value'];
            } else {
                $value = $temp;
            }

            if (!empty($value)) {
                $temp = $value;

                if ($date->lt($dateStart)) {
                    continue;
                }

                $this->insert[] = [
                    'id' => Str::uuid(),
                    'code' => $cbCurrency['CharCode'],
                    'date' => $date,
                    'value' => round((float) str_replace(',', '.', $value), 2),
                ];

                $dateFormatted = $date->format('Y-m-d');
                $this->dates[$dateFormatted] = $dateFormatted;
                $this->codes[$cbCurrency['CharCode']] = $cbCurrency['CharCode'];
            }
        }
    }

    private function performInsert(): void
    {
        try {
            app(CurrencyRepositoryContract::class)->insert($this->insert);

            $this->insert = [];
        } catch (Exception) {
            // если запрос всё таки упал,
            // то разбиваем на более мелкие части
            // и вставляем каждую по очереди
            $chunks = array_chunk($this->insert, 2500);

            foreach ($chunks as $chunk) {
                app(CurrencyRepositoryContract::class)->insert($chunk);
            }

            $this->insert = [];
        }
    }
}
