<?php

namespace App\Services;

use App\Exceptions\Validation\ValidationFieldsNotFoundException;
use App\Models\User;
use App\Repositories\Contracts\CurrencyRepositoryContract;
use App\Services\Contracts\ValidationRulesServiceContract;

class ValidationRulesService implements ValidationRulesServiceContract
{
    private array $baseValidationRules = [
        'login' => [
            'required',
            'string',
            'min:6',
            'max:255',
        ],
        'password' => [
            'required',
            'string',
            'min:6',
        ],
        'code' => [
            'required',
            'string',
            'size:3',
        ],
        'date_start' => [
            'required',
            'date',
            'date_format:Y-m-d',
            'before:date_end',
        ],
        'date_end' => [
            'required',
            'date',
            'date_format:Y-m-d',
            'after:date_start',
        ],
        'order' => [
            'string',
            'in:asc,desc',
        ],
    ];

    private array $additionalValidationRules = [
        'registration' => [
            'login' => [
                'unique:'.User::class.',login',
            ],
            'password' => [
                'confirmed',
            ],
        ],
    ];

    private array $processValidationRules = [
        'code' => [
            'in' => [
                'class' => CurrencyRepositoryContract::class,
                'method' => 'unique',
                'field' => 'code',
                'after' => 'implode',
                'after_data' => ',',
            ],
        ],
    ];

    /**
     * @param  array       $fields
     * @param  string|null $modifier
     *
     * @return array
     *
     * @throws ValidationFieldsNotFoundException
     */
    public function getRules(array $fields, ?string $modifier = null): array
    {
        $validationRules = [];
        $errors = [];

        foreach ($fields as $field) {
            if (empty($this->baseValidationRules[$field])) {
                $errors[] = $field;
            }

            if (!empty($errors)) {
                continue;
            }

            $validationRules[$field] = $this->baseValidationRules[$field];

            if (!empty($this->additionalValidationRules[$modifier][$field])) {
                $validationRules[$field] = array_merge(
                    $validationRules[$field],
                    $this->additionalValidationRules[$modifier][$field]
                );
            }

            if (!empty($this->processValidationRules[$field])) {
                foreach ($this->processValidationRules[$field] as $rule => $ruleData) {
                    $validationRules[$field][] = $rule.':'.app($ruleData['class'])
                        ->{$ruleData['method']}($ruleData['field'])
                        ->{$ruleData['after']}($ruleData['field'], $ruleData['after_data'])
                    ;
                }
            }
        }

        if (!empty($errors)) {
            throw new ValidationFieldsNotFoundException($errors);
        }

        return $validationRules;
    }
}
