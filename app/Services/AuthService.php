<?php

namespace App\Services;

use App\Exceptions\Auth\IncorrectCredentialsException;
use App\Models\Contracts\HasApiTokensContract;
use App\Repositories\Contracts\UserRepositoryContract;
use App\Services\Contracts\AuthServiceContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class AuthService implements AuthServiceContract
{
    public function register(array $data): Model
    {
        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        $user = app(UserRepositoryContract::class)->create($data);

        return app(UserRepositoryContract::class)->createToken($user);
    }

    /**
     * @throws IncorrectCredentialsException
     */
    public function login(array $data): Model
    {
        if (!auth()->attempt($data)) {
            throw new IncorrectCredentialsException();
        }

        $user = auth()->user();

        return app(UserRepositoryContract::class)->createToken($user);
    }

    public function logout(HasApiTokensContract $user): void
    {
        app(UserRepositoryContract::class)->revokeCurrentToken($user);
    }
}
