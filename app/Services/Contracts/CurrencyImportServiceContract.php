<?php

namespace App\Services\Contracts;

interface CurrencyImportServiceContract
{
    public function importFromCB(): void;
}
