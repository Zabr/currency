<?php

namespace App\Services\Contracts;

interface ValidationRulesServiceContract
{
    public function getRules(array $fields, ?string $modifier = null): array;
}
