<?php

namespace App\Services\Contracts;

use App\Models\Contracts\HasApiTokensContract;
use Illuminate\Database\Eloquent\Model;

interface AuthServiceContract
{
    public function register(array $data): Model;

    public function login(array $data): Model;

    public function logout(HasApiTokensContract $user): void;
}
