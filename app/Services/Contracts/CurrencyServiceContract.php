<?php

namespace App\Services\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface CurrencyServiceContract
{
    public function current(): Collection;

    public function history(array $data): Collection;
}
