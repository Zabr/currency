<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface CurrencyRepositoryContract
{
    public function select(array $data): Collection;
}
