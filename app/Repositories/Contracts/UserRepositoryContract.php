<?php

namespace App\Repositories\Contracts;

use App\Models\Contracts\HasApiTokensContract;
use Illuminate\Database\Eloquent\Model;

interface UserRepositoryContract
{
    public function createToken(HasApiTokensContract $user): Model;

    public function revokeCurrentToken(HasApiTokensContract $user): void;
}
