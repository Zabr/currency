<?php

namespace App\Repositories\Contracts;

interface CBRepositoryContract
{
    public function getDaily(?string $date = null);

    public function getPeriod(string $code, string $dateStart, string $dateEnd);
}
