<?php

namespace App\Repositories;

use App\Cache\CBRepositoryCacheKeys;
use App\Repositories\Contracts\CBRepositoryContract;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class CBRepository implements CBRepositoryContract
{
    use CBRepositoryCacheKeys;

    private array $config;

    private $client;

    public function __construct()
    {
        $this->config = config('cb');

        $this->client = Http::baseUrl($this->config['base_url']);
    }

    public function getDaily(?string $date = null)
    {
        return Cache::remember(
            $this->getDailyCacheKey($date),
            60 * 60,
            function () use ($date) {
                return $this->client->get('XML_daily.asp', [
                    'date_req' => $date,
                ])->body();
            }
        );
    }

    public function getPeriod(string $code, string $dateStart, string $dateEnd)
    {
        return Cache::remember(
            $this->getPeriodCacheKey($code, $dateStart, $dateEnd),
            60 * 60,
            function () use ($code, $dateStart, $dateEnd) {
                return $this->client->get('XML_dynamic.asp', [
                    'VAL_NM_RQ' => $code,
                    'date_req1' => $dateStart,
                    'date_req2' => $dateEnd,
                ])->body();
            }
        );
    }
}
