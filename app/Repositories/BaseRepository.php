<?php

namespace App\Repositories;

use App\Cache\BaseRepositoryCacheKeys;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

abstract class BaseRepository
{
    use BaseRepositoryCacheKeys;

    protected Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    public function insert(array $data): bool
    {
        return $this->model->insert($data);
    }

    public function where(string $field, $value): Collection
    {
        return Cache::remember(
            $this->whereCacheKey(get_class($this->model), $field, $value),
            60 * 60,
            function () use ($field, $value) {
                return $this->model->where($field, $value)->get();
            }
        );
    }

    public function unique($field): Collection
    {
        return Cache::remember(
            $this->uniqueCacheKey(get_class($this->model), $field),
            60 * 60,
            function () use ($field) {
                return $this->model->select($field)->distinct()->get();
            }
        );
    }
}
