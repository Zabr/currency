<?php

namespace App\Repositories;

use App\Models\Contracts\HasApiTokensContract;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Database\Eloquent\Model;

class UserRepository extends BaseRepository implements UserRepositoryContract
{
    public function createToken(HasApiTokensContract $user): Model
    {
        $user['token'] = $user->createToken('api');

        return $user;
    }

    public function revokeCurrentToken(HasApiTokensContract $user): void
    {
        $user->currentAccessToken()->delete();
    }
}
