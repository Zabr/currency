<?php

namespace App\Repositories;

use App\Cache\CurrencyRepositoryCacheKeys;
use App\Repositories\Contracts\CurrencyRepositoryContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class CurrencyRepository extends BaseRepository implements CurrencyRepositoryContract
{
    use CurrencyRepositoryCacheKeys;

    public function select(array $data): Collection
    {
        return Cache::tags([$this->selectCacheTag()])->remember(
            $this->selectCacheKey($data),
            60 * 60,
            function () use ($data) {
                $query = $this->model->query();

                if (!empty($data['code'])) {
                    $query->where('code', $data['code']);
                }

                if (!empty($data['date_start'])) {
                    $query->whereDate('date', '>=', $data['date_start']);
                }

                if (!empty($data['date_end'])) {
                    $query->whereDate('date', '<=', $data['date_end']);
                }

                if (!empty($data['order'])) {
                    $query->orderBy('date', $data['order']);
                }

                return $query->get();
            }
        );
    }
}
