<?php

if (!function_exists('xmlToArray')) {
    function xmlToArray($xml)
    {
        return json_decode(json_encode(simplexml_load_string($xml, null, LIBXML_NOCDATA)), true);
    }
}
