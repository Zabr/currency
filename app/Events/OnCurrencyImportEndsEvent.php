<?php

namespace App\Events;

class OnCurrencyImportEndsEvent
{
    public ?array $dates;
    public ?array $codes;

    public function __construct(?array $dates, ?array $codes)
    {
        $this->dates = $dates;
        $this->codes = $codes;
    }
}
