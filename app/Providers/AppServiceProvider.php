<?php

namespace App\Providers;

use App\Models\Currency;
use App\Models\User;
use App\Repositories\CBRepository;
use App\Repositories\Contracts\CBRepositoryContract;
use App\Repositories\Contracts\CurrencyRepositoryContract;
use App\Repositories\Contracts\UserRepositoryContract;
use App\Repositories\CurrencyRepository;
use App\Repositories\UserRepository;
use App\Services\AuthService;
use App\Services\Contracts\AuthServiceContract;
use App\Services\Contracts\CurrencyImportServiceContract;
use App\Services\Contracts\CurrencyServiceContract;
use App\Services\Contracts\ValidationRulesServiceContract;
use App\Services\CurrencyImportService;
use App\Services\CurrencyService;
use App\Services\ValidationRulesService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->registerHelpers();
        $this->registerServices();
        $this->registerRepositories();
    }

    private function registerHelpers(): void
    {
        require_once app()->basePath('app/Helpers/System.php');

        $env = config('app.env');
        if ($env === 'local' || $env === 'dev' || $env === 'testing') {
            require_once app()->basePath('app/Helpers/Dev.php');
        }
    }

    private function registerServices(): void
    {
        $this->app->singleton(ValidationRulesServiceContract::class, ValidationRulesService::class);

        $this->app->singleton(AuthServiceContract::class, AuthService::class);

        $this->app->singleton(CurrencyImportServiceContract::class, CurrencyImportService::class);

        $this->app->singleton(CurrencyServiceContract::class, CurrencyService::class);
    }

    private function registerRepositories(): void
    {
        $this->app->bind(UserRepositoryContract::class, function () {
            return new UserRepository(new User());
        });

        $this->app->bind(CurrencyRepositoryContract::class, function () {
            return new CurrencyRepository(new Currency());
        });

        $this->app->singleton(CBRepositoryContract::class, CBRepository::class);
    }
}
