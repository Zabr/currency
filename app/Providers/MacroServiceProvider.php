<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * Register macros
     */
    public function boot(): void
    {
        $this->registerResponseMacro();
    }

    private function registerResponseMacro(): void
    {
        Response::macro('apiResponse', function ($data = null, $errorCode = null, $errorMessage = null) {
            $hasError = !is_null($errorCode);
            if ($hasError) {
                $content = [
                    'error_code' => $errorCode,
                    'error_message' => $errorMessage ?? trans('error.default_error'),
                    'error_data' => $data,
                ];
            } else {
                $content = $data;
            }

            return Response::json([
                'error' => $hasError,
                'content' => $content,
            ]);
        });
    }
}
