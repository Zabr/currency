<?php

namespace App\Console\Commands;

use App\Services\Contracts\CurrencyImportServiceContract;
use Illuminate\Console\Command;

class ImportCurrency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Заполняет недостающие котировки';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        app(CurrencyImportServiceContract::class)->importFromCB();
    }
}
