<?php

namespace App\Cache;

trait CBRepositoryCacheKeys
{
    private function getDailyCacheKey(?string $date): string
    {
        return 'getDaily:'.$date;
    }

    private function getPeriodCacheKey(string $code, string $dateStart, string $dateEnd): string
    {
        return 'getPeriod:'.$code.':'.$dateStart.':'.$dateEnd;
    }
}
