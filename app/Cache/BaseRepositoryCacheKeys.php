<?php

namespace App\Cache;

trait BaseRepositoryCacheKeys
{
    private function whereCacheKey(string $model, string $field, $value): string
    {
        return 'where:'.$model.':'.$field.':'.serialize($value);
    }

    private function uniqueCacheKey(string $model, string $field): string
    {
        return 'unique:'.$model.':'.$field;
    }
}
