<?php

namespace App\Cache;

trait CurrencyRepositoryCacheKeys
{
    private function selectCacheTag(): string
    {
        return 'CurrencySelectTag';
    }

    private function selectCacheKey(array $data): string
    {
        return 'CurrencySelectKey:'.serialize($data);
    }
}
