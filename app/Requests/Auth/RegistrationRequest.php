<?php

namespace App\Requests\Auth;

use App\Requests\BaseRequest;
use App\Services\Contracts\ValidationRulesServiceContract;

class RegistrationRequest extends BaseRequest
{
    public function rules(): array
    {
        return app(ValidationRulesServiceContract::class)->getRules(['login', 'password'], 'registration');
    }
}
