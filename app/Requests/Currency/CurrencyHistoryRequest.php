<?php

namespace App\Requests\Currency;

use App\Requests\BaseRequest;
use App\Services\Contracts\ValidationRulesServiceContract;

class CurrencyHistoryRequest extends BaseRequest
{
    public function rules(): array
    {
        return app(ValidationRulesServiceContract::class)->getRules(['code', 'date_start', 'date_end', 'order']);
    }
}
