<?php

namespace App\Exceptions;

use App\Exceptions\Auth\UnauthenticatedException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    private array $exceptionMapping = [
        AuthenticationException::class => UnauthenticatedException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     * @param  Throwable $e
     *
     * @throws Throwable
     */
    public function report(Throwable $e): void
    {
        if (!($e instanceof BaseException)) {
            parent::report($e);
        }
    }

    /**
     * Render an exception into an HTTP response.
     * @param  Request   $request
     * @param  Throwable $e
     *
     * @return Response|JsonResponse
     *
     * @throws Throwable
     */
    public function render($request, Throwable $e): Response|JsonResponse
    {
        if (!empty($this->exceptionMapping[get_class($e)])) {
            $e = new $this->exceptionMapping[get_class($e)]();
        }

        if ($e instanceof BaseException) {
            if ($request->expectsJson()) {
                return response()->apiResponse(
                    $e->getExceptionData(),
                    $e->getExceptionCode(),
                    $e->getMessage()
                );
            }
        }

        return parent::render($request, $e);
    }
}
