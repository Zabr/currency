<?php

namespace App\Exceptions\Validation;

use App\Exceptions\BaseException;

class ValidationException extends BaseException
{
    protected const SYMBOLIC_CODE = 'validation_error';
}
