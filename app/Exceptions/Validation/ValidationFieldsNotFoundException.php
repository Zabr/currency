<?php

namespace App\Exceptions\Validation;

use App\Exceptions\BaseException;

class ValidationFieldsNotFoundException extends BaseException
{
    protected const SYMBOLIC_CODE = 'validation_fields_not_found_error';
}
