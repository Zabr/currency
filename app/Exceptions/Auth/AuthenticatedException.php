<?php

namespace App\Exceptions\Auth;

use App\Exceptions\BaseException;

class AuthenticatedException extends BaseException
{
    protected const SYMBOLIC_CODE = 'authenticated_error';
}
