<?php

namespace App\Exceptions\Auth;

use App\Exceptions\BaseException;

class UnauthenticatedException extends BaseException
{
    protected const SYMBOLIC_CODE = 'unauthenticated_error';
}
