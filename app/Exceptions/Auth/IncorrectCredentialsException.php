<?php

namespace App\Exceptions\Auth;

use App\Exceptions\BaseException;

class IncorrectCredentialsException extends BaseException
{
    protected const SYMBOLIC_CODE = 'incorrect_credentials_error';
}
