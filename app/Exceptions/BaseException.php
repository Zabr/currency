<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class BaseException extends Exception
{
    protected const SYMBOLIC_CODE = 'undefined_error';

    private mixed $data;

    public function __construct($data = null, $code = 0, Throwable $previous = null)
    {
        parent::__construct(trans('errors.'.$this->getExceptionCode()), $code, $previous);

        $this->data = $data;
    }

    public function getExceptionCode(): string
    {
        return static::SYMBOLIC_CODE;
    }

    public function getExceptionData(): mixed
    {
        return $this->data;
    }
}
