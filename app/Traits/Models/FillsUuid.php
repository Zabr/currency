<?php

namespace App\Traits\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait FillsUuid
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model[$model->getKeyName()] = (string) Str::uuid();
        });
    }
}
