[![pipeline status](https://gitlab.com/Zabr/currency/badges/main/pipeline.svg)](https://gitlab.com/Zabr/currency/-/commits/main) [![coverage report](https://gitlab.com/Zabr/currency/badges/main/coverage.svg)](https://gitlab.com/Zabr/currency/-/commits/main)

## Сборка проекта

1. Клонируем проект;

`git clone git@gitlab.com:Zabr/currency.git`

2. Заходим в склонированную папку и поднимаем контейнеры;

`cd currency/ && docker-compose up -d`

Чтобы корректно спулился образ для контейнера **currency**, у вас должен быть зарегистрирован токен для **registry.gitlab.com**, это можно сделать командой:

`docker login -u gitlab-ci-token -p <access_token> registry.gitlab.com`

предварительно создав токен [тут](https://gitlab.com/-/profile/personal_access_tokens) с правами на **read_registry**

Если с этим возникли какие-либо трудности, то можно заменить использование образа на его сборку, закомментировав в **docker-compose.yml** в сервисе **currency** ключ **image** и раскомментировав блок **build**, после чего выполнить:

`docker-compose build`

и запустить готовый образ:

`docker-compose up -d`

3. Заходим в контейнер **currency**;

`docker exec -it currency bash`

4. Устанавливаем **composer**;

`composer install`

5. Накатываем миграции;

`php artisan migrate --force`

6. Запускаем первичный импорт (~1 мин);

`php artisan currency:import`

7. Выходим из **currency**;

`exit`

8. Перезапускаем контейнер **schedule** (поскольку он был нерабочим без композера);

`docker-compose stop schedule && docker-compose up -d schedule`

Импорт запускается автоматически каждый час (в 0 минут) в отдельном контейнере **schedule** и заполняет отсутствующие курсы.

## Методы API

Все методы имеют шаблон ответа:

| поле | тип | описание |
| ------ | ------ | ------ |
| error | boolean | ошибка или нет |
| content | object | данные по ошибке или результат запроса |

См. [MacroServiceProvider.php](https://gitlab.com/Zabr/currency/-/blob/main/app/Providers/MacroServiceProvider.php)

В дальнейшем будет описываться содержимое **content**

В методы под авторизацией нужно добавить хедер:

`Authorization: Bearer <token>`

В каждый из методов можно добавить параметр:

| поле | валидация | описание |
| ------ | ------ | ------ |
| lang | [ru, en] | язык ошибок |

чтобы получать ошибки на указанном языке, по умолчанию **ru**

### Регистрация нового пользователя

`POST /api/auth/register`

Запрос:

| поле | валидация | описание |
| ------ | ------ | ------ |
| login | обязательное, 6-255 символов | логин пользователя |
| password | обязательное, 6+ символов | пароль пользователя |
| password_confirmation |  | подтверждение пароля |

См. [RegistrationRequest.php](https://gitlab.com/Zabr/currency/-/blob/main/app/Requests/Auth/RegistrationRequest.php)

Ответ:

| поле | тип | описание |
| ------ | ------ | ------ |
| id | string (uuid) | Id пользователя |
| login | string | логин пользователя |
| token | string | токен пользователя |

См. [UserWithTokenPresenter.php](https://gitlab.com/Zabr/currency/-/blob/main/app/Presenters/Auth/UserWithTokenPresenter.php)

### Авторизация

`POST /api/auth/login`

Запрос:

| поле | валидация | описание |
| ------ | ------ | ------ |
| login | обязательное, 6-255 символов | логин пользователя |
| password | обязательное, 6+ символов | пароль пользователя |

См. [LoginRequest.php](https://gitlab.com/Zabr/currency/-/blob/main/app/Requests/Auth/LoginRequest.php)

Ответ:

| поле | тип | описание |
| ------ | ------ | ------ |
| id | string (uuid) | Id пользователя |
| login | string | логин пользователя |
| token | string | токен пользователя |

См. [UserWithTokenPresenter.php](https://gitlab.com/Zabr/currency/-/blob/main/app/Presenters/Auth/UserWithTokenPresenter.php)

### Получение текущих курсов валют

`GET /api/currency/current`

Под авторизацией, нет параметров в запросе.

Ответ:

| поле | тип | описание |
| ------ | ------ | ------ |
| code | string | код валюты |
| date | date | дата, на которую актуален курс |
| value | float | значение курса |

См. [CurrencyPresenter.php](https://gitlab.com/Zabr/currency/-/blob/main/app/Presenters/Currency/CurrencyPresenter.php)

### Получение истории изменения курса конкретной валюты

`GET /api/currency/history`

Под авторизацией.

Запрос:

| поле | валидация | описание |
| ------ | ------ | ------ |
| code | обязательное, 3 символа, из имеющихся | код валюты |
| date_start | обязательное, дата в формате Y-m-d, раньше date_end | дата начала периода |
| date_end | обязательное, дата в формате Y-m-d, позже date_start | дата конца периода |
| order | [asc, desc] | направление сортировки |

См. [CurrencyHistoryRequest.php](https://gitlab.com/Zabr/currency/-/blob/main/app/Requests/Currency/CurrencyHistoryRequest.php)

Ответ:

| поле | тип | описание |
| ------ | ------ | ------ |
| code | string | код валюты |
| date | date | дата, на которую актуален курс |
| value | float | значение курса |

См. [CurrencyPresenter.php](https://gitlab.com/Zabr/currency/-/blob/main/app/Presenters/Currency/CurrencyPresenter.php)

### Выход

`POST /api/auth/logout`

Под авторизацией, нет параметров в запросе и ответе.

## Комментарии

Поскольку одним из условий было выдерживать высокую нагрузку, отказался от идеи проксирования запросов и пришёл к варианту импорт + работа с нашей БД + кеширование.

У курсов ЦБ много пропусков по дням, если курс на какую-то дату отсутствует, то для этой даты используется ближайший курс предыдущих дней.

## Ответы на возможные вопросы:

#### Что за номера в коммитах ветках?

Это номера задач, на которые я декомпозировал ТЗ.

В целом декомпозиция выглядела так:

| номер | задача |
| ------ | ------ |
| 1 | Развернуть чистый Laravel проект, почистить от мусора |
| 2 | Базовые вещи для работы (хэндлер эксепшонов, макросы, хелперы, базовые реквест, экспешн, презентер и тд) |
| 3 | Пользователи (регистрация, авторизация, выход) |
| 4 | Импорт курсов валют из ЦБ к нам в БД |
| 5 | Методы получения курсов через API |
| 6 | Настроить контейнеры, docker-compose, gitlab-ci |
| 7 | Написать readme, QC проекта |
