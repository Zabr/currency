<?php

namespace Tests\Unit\Http\Controllers;

use App\Http\Controllers\CurrencyController;
use App\Requests\Currency\CurrencyHistoryRequest;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class CurrencyControllerTest extends TestCase
{
    public function testCurrent(): void
    {
        self::assertInstanceOf(
            JsonResponse::class,
            app(CurrencyController::class)->current()
        );
    }

    public function testHistory(): void
    {
        self::assertInstanceOf(
            JsonResponse::class,
            app(CurrencyController::class)->history(
                $this->getRequest(CurrencyHistoryRequest::class, [
                    'code' => 'aud',
                    'date_start' => '2022-01-01',
                    'date_end' => '2023-01-01',
                    'order' => 'desc',
                ])
            )
        );
    }
}
