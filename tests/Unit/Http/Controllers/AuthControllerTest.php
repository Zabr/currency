<?php

namespace Tests\Unit\Http\Controllers;

use App\Exceptions\Auth\IncorrectCredentialsException;
use App\Exceptions\Validation\ValidationException;
use App\Http\Controllers\AuthController;
use App\Requests\Auth\LoginRequest;
use App\Requests\Auth\RegistrationRequest;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    public function testValidationException(): void
    {
        $this->expectException(ValidationException::class);

        self::assertInstanceOf(
            JsonResponse::class,
            app(AuthController::class)->register(
                $this->getRequest(RegistrationRequest::class, [
                    'login' => 'login',
                    'password' => 'password',
                    'password_confirmation' => '123',
                ])
            )
        );
    }

    public function testRegister(): void
    {
        self::assertInstanceOf(
            JsonResponse::class,
            app(AuthController::class)->register(
                $this->getRequest(RegistrationRequest::class, [
                    'login' => 'login1',
                    'password' => 'password',
                    'password_confirmation' => 'password',
                ])
            )
        );
    }

    public function testLogin(): void
    {
        self::assertInstanceOf(
            JsonResponse::class,
            app(AuthController::class)->login(
                $this->getRequest(LoginRequest::class, [
                    'login' => 'login1',
                    'password' => 'password',
                ])
            )
        );
    }

    public function testLoginFail(): void
    {
        $this->expectException(IncorrectCredentialsException::class);

        self::assertInstanceOf(
            JsonResponse::class,
            app(AuthController::class)->login(
                $this->getRequest(LoginRequest::class, [
                    'login' => 'login1',
                    'password' => '123123',
                ])
            )
        );
    }

    public function testLogout(): void
    {
        $this->setUser();

        self::assertInstanceOf(
            JsonResponse::class,
            app(AuthController::class)->logout()
        );
    }
}
