<?php

namespace Tests\Unit\Http\Middleware;

use App\Exceptions\Auth\AuthenticatedException;
use App\Http\Middleware\AcceptJson;
use App\Http\Middleware\GuestApi;
use App\Http\Middleware\Localization;
use App\Http\Middleware\TrimStrings;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Tests\TestCase;

class MiddlewareTest extends TestCase
{
    public function testAcceptJsonMiddleware(): void
    {
        app(AcceptJson::class)->handle(new Request(), function (Request $req) {
            $this->assertTrue($req->headers->get('accept') === 'application/json');
        });
    }

    public function testGuestApiMiddleware(): void
    {
        app(GuestApi::class)->handle(new Request(), function (Request $req) {
            // чтобы покрыть последнюю строку, когда всё ок и экспешона нет
            $this->assertInstanceOf(Request::class, $req);
        });
    }

    public function testFailGuestApiMiddleware(): void
    {
        $this->setUser();

        $this->expectException(AuthenticatedException::class);

        app(GuestApi::class)->handle(new Request(), function () {});
    }

    public function testLocalizationMiddleware(): void
    {
        app(Localization::class)->handle(new Request(['lang' => 'en']), function () {
            $this->assertTrue(app()->getLocale() === 'en');
        });
    }

    public function testTrimStringsMiddleware(): void
    {
        app(TrimStrings::class)->handle(new Request(['value_for_test' => 'value   ']), function (Request $req) {
            $this->assertTrue($req->get('value_for_test') === 'value');
        });
    }

    public function testDontTrimStringsMiddleware(): void
    {
        app(TrimStrings::class)->handle(new Request(['password' => 'value   ']), function (Request $req) {
            $this->assertTrue($req->get('password') === 'value   ');
        });
    }

    public function testThrottleRequestsMiddleware(): void
    {
        $response = app(ThrottleRequests::class)->handle(new Request(), function () {
            return new Response();
        }, 'api');

        self::assertTrue($response->headers->has('x-ratelimit-limit'));
        self::assertTrue($response->headers->has('x-ratelimit-remaining'));
    }
}
