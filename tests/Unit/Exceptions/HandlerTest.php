<?php

namespace Tests\Unit\Exceptions;

use App\Exceptions\BaseException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tests\TestCase;

class HandlerTest extends TestCase
{
    public function testReport(): void
    {
        self::assertNull(app(ExceptionHandler::class)->report(new Exception()));
    }

    public function testBaseExceptionReport(): void
    {
        self::assertNull(app(ExceptionHandler::class)->report(new BaseException()));
    }

    public function testRender(): void
    {
        self::assertInstanceOf(
            JsonResponse::class,
            app(ExceptionHandler::class)->render($this->getJsonRequest(), new Exception())
        );
    }

    public function testBaseExceptionRender(): void
    {

        self::assertInstanceOf(
            JsonResponse::class,
            app(ExceptionHandler::class)->render($this->getJsonRequest(), new BaseException())
        );
    }

    public function testMappedExceptionRender(): void
    {
        self::assertInstanceOf(
            JsonResponse::class,
            app(ExceptionHandler::class)->render($this->getJsonRequest(), new AuthenticationException())
        );
    }

    private function getJsonRequest(): array|string|Request|Application|null
    {
        $request = request();
        $request->headers->set('Accept', 'application/json');

        return $request;
    }
}
