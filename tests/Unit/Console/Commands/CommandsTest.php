<?php

namespace Tests\Unit\Console\Commands;

use App\Repositories\Contracts\CBRepositoryContract;
use App\Repositories\Contracts\CurrencyRepositoryContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Http;
use Mockery\MockInterface;
use Tests\TestCase;

class CommandsTest extends TestCase
{
    private const DAILY_XML = '<?xml version="1.0" encoding="windows-1251"?>'.
    '<ValCurs Date="20.01.2022" name="Foreign Currency Market">'.
    '<Valute ID="R01010"><CharCode>AUD</CharCode><Value>55,2078</Value></Valute>'.
    '<Valute ID="R01020A"><CharCode>AZN</CharCode><Value>45,2441</Value></Valute>'.
    '</ValCurs>';

    private const PERIOD_XML = '<?xml version="1.0" encoding="windows-1251"?>'.
    '<ValCurs ID="R01010" DateRange1="01.07.1992" DateRange2="01.07.2005">'.
    '<Record Date="01.07.1992" Id="R01010"><Value>118,3000</Value></Record>'.
    '<Record Date="01.07.2000" Id="R01010"><Value>116,8400</Value></Record>'.
    '</ValCurs>';

    public function testSchedule(): void
    {
        $this->artisan('schedule:list')->assertExitCode(0);
    }

    public function testImportCurrency(): void
    {
        Http::shouldReceive('baseUrl')->andReturnSelf();
        Http::shouldReceive('get')->andReturnSelf();
        Http::shouldReceive('body')->andReturn(self::DAILY_XML);

        $this->artisan('currency:import')->assertExitCode(0);
    }

    public function testImportCurrency2(): void
    {
        $this->mock(CBRepositoryContract::class, function (MockInterface $mock) {
            $mock->shouldReceive('getDaily')->andReturn(self::DAILY_XML);
            $mock->shouldReceive('getPeriod')->andReturn(self::PERIOD_XML);
        });

        $this->mock(CurrencyRepositoryContract::class, function (MockInterface $mock) {
            $mock->shouldReceive('where')->andReturn(new Collection());
            $mock->shouldReceive('insert')->andReturn(true);
        });

        $this->artisan('currency:import')->assertExitCode(0);
    }

    public function testImportCurrency3(): void
    {
        $this->mock(CBRepositoryContract::class, function (MockInterface $mock) {
            $mock->shouldReceive('getDaily')->andReturn(self::DAILY_XML);
            $mock->shouldReceive('getPeriod')->andReturn(self::PERIOD_XML);
        });

        $this->artisan('currency:import')->assertExitCode(0);
    }
}
