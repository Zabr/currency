<?php

namespace Tests\Unit\Services;

use App\Exceptions\Validation\ValidationFieldsNotFoundException;
use App\Services\Contracts\ValidationRulesServiceContract;
use Tests\TestCase;

class ValidationRuleServiceTest extends TestCase
{
    public function testGetRulesFieldsException(): void
    {
        $this->expectException(ValidationFieldsNotFoundException::class);

        app(ValidationRulesServiceContract::class)->getRules(['non_existing_field']);
    }
}
