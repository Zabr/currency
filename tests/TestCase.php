<?php

namespace Tests;

use App\Models\User;
use App\Requests\BaseRequest;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Sanctum\PersonalAccessToken;

abstract class TestCase extends BaseTestCase
{
    /**
     * Создаёт объект приложения
     */
    public function createApplication(): Application
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Создаёт объект реквеста
     */
    protected function getRequest(string $requestName, array $data = []): BaseRequest
    {
        $request = (new $requestName($data))->setContainer(app());

        $request->validateResolved();

        return $request;
    }

    /**
     * устанавливает юзера
     */
    protected function setUser(array $attributes = []): void
    {
        $user = new User($attributes);
        $user->withAccessToken(new PersonalAccessToken());
        auth('sanctum')->setUser($user);
        auth()->shouldUse('sanctum');
    }
}
